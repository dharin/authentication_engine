require_dependency "authentication_engine/application_controller"

module AuthenticationEngine
  class UsersController < ApplicationController

    before_action :find_user, only: [:edit, :update, :destroy, :show]

  	def index
  		@user = User.all
  	end

  	def new
  		@user = User.new
  	end

    def show
      unless @user.present?
        redirect_to root_path, notice: 'Invalid User'
      end      
    end

  	def create
      @user = User.new(user_params)
      if @user.save
      	session[:user_id] = @user.id
        redirect_to root_path, notice: 'User was successfully created.'
      else
        render :new
      end  		
  	end

    def edit
      unless @user.present?
        redirect_to root_path, notice: 'Invalid User'
      end
    end


    def update
      if @user.update(user_params)
        redirect_to root_path, notice: 'User was successfully created.'
      else
        render action: :edit
      end
    end


  	def sign_in
  		if request.post?
	  		@user = User.find_by(email: params[:email])
	  		if @user.present? && @user.valid_password?(params[:password])
	  			session[:user_id] = @user.id
	  			redirect_to root_path, notice: 'Successfully log in'
	  		else
	  			redirect_to '/users/sign_in', notice: 'Invalid User'
	  		end
	  	end
  	end

  	def sign_out
  		session[:user_id] = nil
  		redirect_to root_path, notice: 'Successfully logout'
  	end

  	private

    def find_user
      @user = User.find_by(id: params[:id])
    end

  	def user_params
  		params.require(:user).permit(:fullname, :username, :email, :password, :password_confirmation)
  	end

  end
end
