module AuthenticationEngine
  class User < ApplicationRecord
    # Include default devise modules. Others available are:
    # :confirmable, :lockable, :timeoutable and :omniauthable
    devise :database_authenticatable, :registerable, :recoverable, :rememberable, :trackable, :validatable

    validates :fullname, :username, presence: true

    def email_or_fullname_or_username
    	fullname || username || email
    end
  end
end
