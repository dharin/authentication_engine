$:.push File.expand_path("../lib", __FILE__)

# Maintain your gem's version:
require "authentication_engine/version"

# Describe your gem and declare its dependencies:
Gem::Specification.new do |s|
  s.name        = "authentication_engine"
  s.version     = AuthenticationEngine::VERSION
  s.authors     = ["dharin"]
  s.email       = ["dharin.rajgor@gmail.com"]
  # s.homepage    = "TODO"
  s.summary     = "Summary of AuthenticationEngine."
  s.description = "Description of AuthenticationEngine."
  s.license     = "MIT"

  s.files = Dir["{app,config,db,lib}/**/*", "MIT-LICENSE", "Rakefile", "README.md"]

  s.add_dependency "rails", "~> 5.0.2"
  s.add_dependency "devise"

  s.add_development_dependency "sqlite3"
end
