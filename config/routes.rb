AuthenticationEngine::Engine.routes.draw do	
  root to: "welcome#index"
  resources :users, except: [:delete] do
  	collection do
  		get :sign_in
  		delete :sign_out
  		post :sign_in
  	end
  end
end
